# version 1.1.0 2021-01-16
from os import getenv
from os.path import splitext, exists, pathsep, dirname, normpath
import re
import json
from glob import glob


ALL = '[?]'
NONE = '[?]'
FIRST = '[0]'
PREVIOUS = '[x-1]'
NEXT = '[x+1]'
LAST = '[-1]'
LAST_NEXT = '[x+1][-1]'

FILTERS = [ALL, NONE, FIRST, PREVIOUS, NEXT, LAST, LAST_NEXT]

AS_GLOB = '[*]'
AS_REGEX = '[re]'
AS_PERCENT = '[%]'
AS_HASH = '[#]'

PATTERNS = [AS_GLOB, AS_REGEX, AS_PERCENT, AS_HASH]


class Field:
    type = str
    glob_str = '*'
    percent_str = '%s'
    hash_str = '#'
    reg_str = r'([\w:\/\-" +"]+)'

    def format(self, value):
        return value

    def _norm_args(self, key, value, data, types=[]):
        if value in (ALL, LAST_NEXT, None):
            return

        if value not in FILTERS:
            return value

        if (value in FILTERS and data[key] not in types):
            raise Exception('The %s "%s" is not in list %s' %
                            (key, data[key], types))
        elif (types and data[key] not in types):
            raise Exception('The %s "%s" is not in list %s' %
                            (key, value, types))
        elif not types:
            return value

        index = types.index(data[key])
        if value == FIRST:
            return types[0]
        elif value == PREVIOUS:
            return types[max(0, index-1)]
        elif value == NEXT:
            return types[min(len(types)-1, index+1)]
        if value == LAST:
            return types[-1]

        return value

    def sub_reg(self, pattern):
        return pattern.replace(self.raw, self.reg_str, 1)

    def sub_string(self, pattern):
        return pattern

    def sub_glob(self, pattern):
        return pattern.replace(self.raw, self.glob_str, 1)

    def sub_percent(self, pattern):
        return pattern.replace(self.raw, self.percent_str, 1)

    def sub_hash(self, pattern):
        return pattern.replace(self.raw, self.hash_str, 1)

    def __repr__(self):
        return 'Field(%s)' % self.reg_pattern


class FieldEnv(Field):
    def __init__(self, name):
        self.name = name
        self.raw = '{%s}'%name

    def sub_string(self, pattern):
        return pattern.replace('{$%s}'%self.name, self.raw, 1)

    def sub_glob(self, pattern):
        return pattern.replace('{%s}'%self.name,
        getenv(self.name, self.glob_str), 1)

    @classmethod
    def from_str(cls, string):
        result = re.findall(r'{\$(\w+)}', string)
        if result:
            return cls(result[0])

    def sub_reg(self, pattern):
        return pattern.replace('{$%s}'%self.name, self.reg_str, 1)

    def _norm_args(self, key, value, data, types=[]):
        if value in (ALL, LAST_NEXT, None):
            return

        return value


class FieldStr(Field):
    def __init__(self, name):
        self.name = name
        self.raw = '{%s}'%name

    @classmethod
    def from_str(cls, string):
        result = re.findall(r'{(\w+)}', string)
        if result:
            return cls(result[0])


class FieldInt(Field):
    def __init__(self, name, padding=1):
        self.type = int
        self.name = name
        self.raw = '{%s:%02dd}'%(name, padding)
        self.glob_str = '?'*padding
        self.reg_str = r'(\d{%d})'%padding
        self.percent_str = '%' + '%02dd'%padding
        self.hash_str = '#'*padding

    @classmethod
    def from_str(cls, string):
        result = re.findall(r'{(\w+):(\d{2}d)}', string)
        if result:
            name, padding_str = result[0]
            padding = len(format(0, padding_str) )
            return cls(name, padding)

    def format(self, value):
        return int(value)

    def _norm_args(self, key, value, data, types=[]):
        if value==LAST:
            return
        elif value == FIRST:
            return 0
        elif value == PREVIOUS:
            return  max(0, data[key] -1)
        elif value == NEXT:
            return data[key] +1

        return value

    def sub_str(self, pattern):
        return pattern.replace(self.raw, '{%s}'%self.name, 1)


class FieldRegex(Field):
    def __init__(self, name, reg='.*?'):
        self.type = str
        self.name = name
        self.raw = '{%s:%s}'%(name, reg)
        self.reg_str = '(%s)'%reg

    @classmethod
    def from_str(cls, string):
        result = re.findall(r'{(\w+):(.*?)}', string)
        if result:
            name, reg = result[0]
            return cls(name, reg)

    def sub_string(self, pattern):
        return pattern.replace(self.raw, '{%s}'%self.name, 1)

    def sub_glob(self, pattern):
        return pattern.replace('{%s}'%self.name, self.glob_str, 1)


class Template:
    def __init__(self, string, types={}, fields=[]):
        reg_field = re.compile(r'{.*?}')

        self.types = types
        self.field_types = [FieldEnv, FieldStr, FieldInt, FieldRegex] + fields
        self.string = string
        self.reg = string.replace('/','\/')
        self.verbose = False

        self.fields = []

        for match in reg_field.findall(self.reg):
            for field_class in self.field_types:
                field = field_class.from_str(match)
                if field:
                    self.fields.append(field)
                    self.reg = field.sub_reg(self.reg)
                    self.string = field.sub_string(self.string)
                    break

        self.reg = re.compile(self.reg)

    def _norm_args(self, data, **kargs):
        if self.verbose: 
            d = data.copy()
            d.update(kargs)
            missing_fields = set(d)-set([f.name for f in self.fields])

            if missing_fields:
                print("Fields : %s are not in the pattern : %s" %
                    (missing_fields, self.string))

        data = data.copy()
        args = kargs.copy()

        for key, value in kargs.items():
            field = next((f for f in self.fields if f.name == key), None)

            types_value = self.types.get(key, [])
            args[key] = field._norm_args(key, value, data, types_value)

        data.update(args)
        return data

    def _filter_data(self, datas, filters, filtered_data):
        if not filters:
            filtered_data.extend(datas)
            return filtered_data

        key, filter, value = list(filters)[0] # Filter one by one

        values = [d[key] for d in datas]

        if filter in (PREVIOUS, NEXT):
            values.append(value)

        values = sorted(list( set(values) ))
        if key in self.types:
            values.sort(key=lambda x: self.types[key].index(x) )

        if filter == ALL:
            for v in set([d[key] for d in datas]):
                new_datas = [d for d in datas if d[key]==v]
                self._filter_data(new_datas, filters[1:], filtered_data)
        elif filter not in FILTERS:
            new_datas = [d for d in datas if d[key] == filter]
        elif filter == LAST:
            new_datas = [d for d in datas if d[key] == values[-1] ]
        elif filter == FIRST:
            new_datas = [d for d in datas if d[key] == values[0]]
        elif filter == PREVIOUS:
            if not value:
                raise('The key %s has to be in the data' % key)
            index = values.index(value)
            filter_value = None if index == 0 else values[index-1]
            new_datas = [d for d in datas if d[key] == filter_value]
        elif filter == NEXT:
            if not value:
                raise('The key %s has to be in the data' % key)
            index = values.index(value)
            filter_value = None if index == len(values)-1 else values[index+1]
            new_datas = [d for d in datas if d[key] == filter_value]
        elif filter == LAST_NEXT:
            new_datas = []
            for d in datas:
                if d[key] == values[-1]:
                    d_dict = d.copy()
                    d_dict[key] = d[key]+1
                    new_datas.append(d_dict)

        return self._filter_data(new_datas, filters[1:], filtered_data)

    def parse(self, string, strict_parsing=False):
        if isinstance(string, dict):
            return string

        string = str(string).replace('\\', '/')

        data = {}
        result = self.reg.findall(string)

        if not result:
            print('Could not parse : %s with pattern : %s' %
                 (string, self.reg))
            return

        if isinstance(result[0], tuple):
            result = result[0]

        if len(result) != len(self.fields):
            print('The number of fields mismatch : %s, pattern : %s, fields : %s' % (
                string, self.reg, self.fields))
            return

        for field, field_value in zip(self.fields, result):
            field_value = field.format(field_value)

            if field.name in data:
                other_value = data[field.name]
                if other_value != field_value:
                    warning = ('The field "%s" as different values: %s, %s for %s' %(
                               field.name, field_value, other_value, string))
                    if strict_parsing:
                        raise Exception(warning)
                    else:
                        print(warning)
            else:
                data[field.name] = field_value

        return data

    def format(self, data={}, **kargs):
        if isinstance(data, str):
            data = self.parse(data)

        data = self._norm_args(data, **kargs)

        pattern = self.string
 
        for field in self.fields:
            if field.name not in data or data[field.name] is None:
                pattern = field.sub_glob(pattern)

            elif data[field.name] == AS_GLOB:
                pattern = field.sub_glob(pattern)

            elif data[field.name] == AS_REGEX:
                pattern = field.sub_reg(pattern)

            elif data[field.name] == AS_PERCENT:
                pattern = field.sub_percent(pattern)

            elif data[field.name] == AS_HASH:
                pattern = field.sub_hash(pattern)

            elif field.type is int and isinstance(data[field.name], str):
                pattern = field.sub_str(pattern)

        return normpath(pattern.format(**data) )

    def find_all(self, data, get=None, **kargs):
        data = self.parse(data)
        norm_args = {k: None if v in FILTERS else v for k,
                     v in kargs.items()}  # replace FILTERS args None by tpl.ALL

        pattern = self.format(data, **norm_args)
        paths = [f.replace('\\','/') for f in sorted( glob(pattern))]

        if not paths:
            return []

        datas = [self.parse(p) for p in paths]

        ## Transform filter argument by value if has type
        filters = [(k, v, data.get(k)) for k, v in kargs.items()]

        datas = self._filter_data(datas, filters, [])

        if get:
            out = [d[get] for d in datas]
        else:
            out = [self.format(d) for d in datas]

        return sorted( list(set(out)) )

    def find(self, data, get=None, **kargs):
        result = self.find_all(data, get=get, **kargs)
        if result:
            return result[0]

    def find_dirs(self, data):
        fields = {f.name: self.types[f.name] for f in
        self.fields if f.name in self.types}

        folders = []
        for field_name, field_values in fields.items():
            other_fields = {k:v for k,v in fields.items() if k!=field_name}
            for field_value in field_values:
                if other_fields:
                    for name, values in other_fields.items():
                        for v in values:
                            d = data.copy()
                            d[field_name] = field_value
                            d[name] = v
                            folders.append(self.format(d))
                else:
                    d = data.copy()
                    d[field_name] = field_value
                    folders.append(self.format(d))

        return list(set(folders))

    def make_dirs(self, data):

        folders = self.find_dirs(data)

        #Create all possible parent folder for a shot_file
        for f in folders:
            parent = dirname(f)
            if not parent:
                os.makedirs(parent)

        return folders

    def __repr__(self):
        return '\nTemplate(\n%s\n)' % self.string


class Templates:
    def __init__(self, template=None, types=[], fields=[]):
        if template is None:
            template = getenv('TEMPLATES')

        if isinstance(template, str):
            template = re.split(pathsep, template)

        if not isinstance(template, (list, tuple) ):
            template = [template]

        if not isinstance(types, (list, tuple) ):
            types = [types]

        self.reg_ref = re.compile(r'{\@(\w+)}')
        self.reg_env = re.compile(r'{\$(\w+)}')
        #reg_list = re.compile(r'{(\w+):\@(\w+)}')

        self.strict_parsing = False
        self.verbose = False
        self.fields = fields

        templates = []
        for template in [self._read_template(t) for t in template]:
            tpl = template
            if isinstance(template, (list, tuple)):
                tpl = template[0]
                if len(template) >= 1:
                    type = template[1]
                    types.append(type)

            templates.append(tpl)

        self.types = self._merge_dict(types) # available value for field
        self.raw = self._merge_dict(templates) # available value for field
        
        self.expanded = self.raw.copy()
        for k, v in self.raw.items():
            self.expanded[k] = self._expand(v)

        self.templates = {k: Template(v, self.types, self.fields)
                          for k, v in self.expanded.items()}  # Init Template class

    def _merge_dict(self, dicts):
        data = {}
        for d in dicts:
            data.update(d)

        return data

    def _read_template_file(self, path):
        print('Read', path)
        if splitext(path)[1] == '.json':
            with open(path, "r") as file:
                return list(json.load(file))
        elif splitext(path)[1] in ('.yaml', '.yml'):
            import yaml
            with open(path, "r") as file:
                return list(yaml.safe_load_all(file))

    def _read_template(self, template):
        if isinstance(template, dict):
            return template

        if not exists(template):
            raise Exception('The template %s does not exist'%template)
            return {}

        return self._read_template_file(template)

    def _expand(self, template):
        ref_fields = self.reg_ref.findall(template)
        while ref_fields:
            for ref_field in ref_fields:
                ref_value = self.expanded.get(ref_field)
                if not ref_value:
                    raise Exception(
                        'The key "%s" is not in the template' % ref_fields)
                template = template.replace('{@%s}'%ref_field, ref_value)

            ref_fields = self.reg_ref.findall(template)

        return template

    def parse(self, path, template_in=None):
        if isinstance(path, dict):
            return path

        if not template_in:
            template_in = self.current

        if template_in not in self.keys():
            raise Exception('%s not in %s' % (template_in, list(self.keys())))

        template = self[template_in]
        return template.parse(path, self.strict_parsing)

    def format(self, data={}, template_in=None, template_out=None, **kargs):
        if not template_in:
            template_in = self.current

        if isinstance(data, str):
            data = self.parse(data, template_in)

        template_out = template_out if template_out else template_in
        template = self[template_out]

        return template.format(data, **kargs)

    def find_all(self, data, template_in=None, template_out=None, **kargs):
        if not template_in:
            template_in = self.current

        if isinstance(data, str):
            data = self.parse(data, template_in)

        template_out = template_out if template_out else template_in
        template = self[template_out]

        return template.find_all(data, **kargs)

    def find(self, data, template_in=None, template_out=None, **kargs):
        result = self.find_all(data, template_in, template_out, **kargs)
        if result:
            return result[0]

    def norm_types(self, data):
        norm_data = data.copy()

        fields = {}
        for t in self.values():
            for f in t.fields:
                fields[f.name] = f.type

        for key, value in data.items():
            if key in fields:
                norm_data[key] = fields[key](value)

            if key not in self.types:
                continue
            v = self.norm_str(value)
            t = {self.norm_str(t): t for t in self.types[key]}

            if v in t:
                norm_data[key] = t[v]

        return norm_data

    def norm_title(self, string):
        string = string.replace('_',' ').replace('-', ' ')
        string = string.strip(' ').title()

        return string

    def norm_str(self, string):
        import unicodedata
        string = string.lower()
        string = string.replace('-', ' ')
        string = re.sub('[ ]+', ' ', string)
        string = re.sub('[ ]+\/[ ]+', '/', string)
        string = string.strip(' ')
        string = string.replace('_','-').replace(' ', '-')
        string = unicodedata.normalize('NFKD', string).encode('ASCII', 'ignore').decode("utf-8")

        return string

    def __repr__(self):
        text = '\nTemplates(\n'
        for k, v in self.items():
            text += "   %s: %s\n" % (k, v.string if v else '')
        text+= ')\n'

        return text

    def __getitem__(self, k):
        return self.templates[k]

    def __setitem__(self, key, value):
        self.raw[key] = value
        self.expanded[key] = self._expand(value)
        self.templates[key] = Template(self.expanded[key], self.types, self.fields)

    def copy(self):
        instance = Templates({}, types=self.types.copy())
        instance.templates = self.templates.copy()

        return instance

    def to_dict(self):
        return {k : v.string for k, v in self.items()}

    def items(self):
        return self.templates.items()

    def keys(self):
        return self.templates.keys()

    def values(self):
        return self.templates.values()

    def __iter__(self):
        return (i for i in self.values())

    def get(self, name):
        return self.templates.get(name)


if __name__ == '__main__':
    #import doliprane as gadget
    #import os

    #env = gadget.Env('acs')
    #templates = gadget.Templates()

    #print(templates)
    #print(templates)
    #print(templates)

    root = "D:/projet_perso/prog/doliprane/doliprane/store/short-film/automated-customer-service"
    file = root+"/ACS/sequences/se000/sh0000/animatic/ACS_se000_sh0000_animatic_v000.blend"

    #blend = "ACS_se000_sh0000_animatic_v000.blend"

    #data = {}

    #path = templates.format(file, 'shot_file', version= gadget.LAST)
    #print('\n','path:', '\n',path,'\n')

    #data  = templates.format(file, 'shot_file',shot_task="test")
    #print('\n','data:', '\n',data,'\n')

    #new_path = templates.format(file, 'shot_file', 'task_dir')
    #print('\n','new_path:', '\n',new_path,'\n')


    # Find all version of a shot task
    #paths = templates.find_all(file, 'shot_file', sequence=gadget.ALL, shot=gadget.ALL, shot_task=gadget.FIRST,version=gadget.FIRST)
    #print('\nFind all version of a shot task')

    # Find first version of a shot ?
    new_path = templates.find(file, 'shot_file', version=FIRST, task='layout')
    print('\nFind first version of a shot task')
    print('Paths:', new_path,'\n')

    # Find previous task for a shot last version
    new_path = templates.find(file, 'shot_file', task=PREVIOUS, version=LAST)
    print('\nFind previous task for a shot last version')
    print('Path:', new_path,'\n')

    # Find next task for a shot version 1
    new_path = templates.find(file, 'shot_file', task=NEXT, version=1)
    print('\nFind next task for a shot version 1')
    print('Path:', new_path,'\n')

    # Find last last task last version of a shot
    new_path = templates.find(file, 'shot_file', task=LAST, version=LAST)
    print('\nFind last task last version of a shot')
    print('Path:', new_path,'\n')


    # Find last last task last version of all shots
    paths = templates.find_all(file, 'shot_file',
        sequence=ALL,
        shot=ALL,
        task=LAST,
        version=LAST)
    print('\nFind last last task last version of all shots')
    print('Paths:', paths,'\n')





    #paths = templates.find_first(file, 'shot_file', task=gadget.ALL)
    #print('\n','new_paths:', '\n',paths,'\n')
