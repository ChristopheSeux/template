Create a single template :

```
from template import Template

path = 'Z:/shots/sh005/lighting/ACS_sh005_lighting_002.blend'

tpl_shot = Template('{root}/shots/sh{shot:04d}/{task}/{project}_sh{shot:04d}_{task}_{version:03d}.blend')
data = tpl_shot.parse(path)
print(data)
#--> {'root' : 'Z:/', 'shot' : 5, 'task': 'lighting', 'version' : 2, 'extension' : 'blend'}

data = {'root' : 'Z:/', 'shot' : 8, 'task': 'layout', 'version' : 6, 'extension' : 'blend'}
path = tpl_shot.format(data)
print(path)
#--> 'Z:/shots/sh008/layout/ACS_sh008_layout_006.blend'
```



Create a Collection of templates :
- From dict :
    
```
from template import Template
from template import ALL, LAST, NEXT_LAST

data = {
    'shot_name' : '{project}_sh{shot:04d}_{task}_{version:03d}.blend'
    'shot_file' : '{root}/shots/sh{shot:04d}/{task}/{@shot_name}'
}

types = {
    'task' : ['layout', 'anim', 'lighting']
}

T = Templates(data, types)

data = T['shot_file'].parse(path)
print(data)
#--> {'root' : 'Z:/', 'shot' : 5, 'task': 'lighting', 'version' : 2, 'extension' : 'blend'}

data = {'root' : 'Z:/', 'shot' : 8, 'task': 'layout', 'version' : 6, 'extension' : 'blend'}
path = T['shot_file'].format(data, task='lighting') # For dynamic field replacement
print(path)
#--> 'Z:/shots/sh008/lighting/ACS_sh008_lighting_006.blend'

# Get the last version of a shot (Useful for incrementing)
path = T['shot_file'].find(data, task='lighting', version=LAST)

# Get the last version of all tasks for all shots 
paths = T['shot_file'].find_all(data, shot=ALL, task=ALL, version=LAST)
```

specified a the types argument allow you to have a more strict parsing ex : ligthing instead of lighting will raise an error

It allow you to use the filters first, last, previous, next etc

{@shot_name} allow you to reuse a template in the same template collection

{$shot_name} allow you to use env variable

{version:03d} allow you to use padding


- From yaml file :

```
store: '{$STORE}'
project_short: 'TEST'
root: '{$STORE}/{@project_short}'
sequence_dir: '{@root}/sequences'
sequence_name: 'sq{sequence:03d}'
version_name: 'v{version:03d}'
shot_dir: '{@sequence_dir}/{@sequence_name}/{@shot_name}'
shot_name: 'sh{shot:03d}'
shot_task_dir: '{@shot_dir}/{shot_task}'
shot_basename: '{@project_short}_{@sequence_name}_{@shot_name}_{shot_task}_{@version_name}.{ext}'
shot_file: '{@shot_task_dir}/{@shot_basename}'
--- ## TYPES
shot_task:
  - animatic
  - layout
  - anim
  - lighting
  - fx
  - rendering
  - compositing

asset_type:
  - chars
  - sets
  - props
  - fx
```

```
from template import Template
import os

# Constuct a template object with the path of the yml
yaml_template = 'template.yml'
T = Templates(yaml_template)

# Constuct a template with multiple files
# each template override the value of the one before it
yaml_template1 = 'template.yml'
yaml_template2 = 'template.yml'
T = Templates([yaml_template1, yaml_template2])


# Constuct a template from an env var (separator allowed ',' or ';')
os.environ['TEMPLATES'] = 'template.yml, template1.yml'
yaml_template = 'template.yml'
T = Templates()

```