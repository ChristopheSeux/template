from .template import Template, Templates
from .template import ALL, NONE, FIRST, PREVIOUS, NEXT, LAST, LAST_NEXT, \
AS_GLOB, AS_REGEX, AS_PERCENT, AS_HASH
